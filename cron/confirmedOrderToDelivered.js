require('dotenv').config()
const db = require('../knex')

;(async () => {
  await db('order')
    .stream(stream => {
      stream.on('data', data => {
        stream.pause()

        ;(async () => {
          console.log(data)
          if (data.status === 'confirmed') {
            await db('order')
              .where('id', data.id)
              .update('status', 'delivered')
          }
        })()

        stream.resume()
      })

      stream.on('end', () => {
        setTimeout(() => {
          db.destroy()
        }, 1000)
      })
    })
})()


// # * * * * * node ~/.../setel/cron/confirmedOrderToDelivered.js
// add this to cronscript with minute interval
