require('dotenv').config()

const express = require('express')
const apiRoute = require('./routes/api')
const app = express()
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended:true
}))

app.use('/api', apiRoute)

app.listen(process.env.NODE_PORT, () => {
  console.log(`server running on port ${process.env.NODE_PORT}`)
})
