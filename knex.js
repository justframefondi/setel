require('dotenv').config()

const knex = require('knex')({
  client: 'pg',
  connection: {
    host:process.env.SETEL_HOST,
    database: process.env.SETEL_DB,
    user: process.env.SETEL_USER,
    password: process.env.SETEL_PW
  }
})

module.exports = knex
