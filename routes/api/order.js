require('dotenv').config()

const express = require('express')
const router = express.Router()
const db = require('../../knex.js')
const moment = require('moment')
const rp = require('request-promise')

//====================//
// API get all orders //
//====================//

router.get('/', async function(req, res) {
  const order = await db('order')
    .select('*')

  res.json(order)
})

//====================//
//API Create new order//
//====================//

router.post('/', async function(req, res) {
  const order = req.body
  const [orderId] = await db('order')
    .insert({
      input: order,
      created: moment().format()
    })
    .returning('id')

  await db('order')
    .where('id', orderId)
    .update('status', 'created')

  if (!order.product) {
    const errmsg = `${order.product} there is a problem with the form sent`

    await db('order')
      .where('id', orderId)
      .update({
        err_msg: errmsg,
        status: 'cancelled',
        deleted: moment().format()
      })

    res.json({
      err: errmsg
    })
  }


  //available for input manipulation HERE
  const currentOrder = await db('order')
    .where('id', orderId)
    .first()

  if (currentOrder.status === 'created') {
    res.json(currentOrder)
  }
})

//====================//
// API Cancel orders  //
//====================//
router.post('/cancel/:id', async function(req, res) {
  const orderId = req.params.id
  const order = await db('order')
    .where('id', orderId)

  if (!order) {
    res.json({msg: 'the order does not exist'})
  }

  if (order.status === 'cancelled') {
    res.json({msg: 'order is already cancelled unable to modify again'})
  }

  if (order.status === 'confirmed' || order.status === 'created') {
    await db('order')
      .where('id', orderId)
      .update({
        status: 'cancelled',
        deleted: moment().format()
      })
    res.json({
      msg: `the order for ${orderId} is now cancelled`
    })
  }
})

//====================//
// API Update orders  //
//====================//

router.post('/update/:id', async function(req, res) {
  const orderId = req.params.id
  const order = await db('order')
    .first()
    .where('id', orderId)

  // if orderId doesnt exist
  if (!order) {
    res.json({msg: 'the order does not exist'})
  }

  if (order.status === 'cancelled') {
    res.json({msg: 'order is already cancelled unable to modify again'})
  }

  const options = {
    uri: process.env.PAYMENT_URL,
    json:true
  }

  const data = await rp(options)

  let paymentStatus

  if (data.result === 1) {
    const orderStatus = await db('order')
      .where('id', orderId)
      .first()

    if (orderStatus.status === 'cancelled') {
      res.json({
        msg: 'cancelled orders cannot be altered'
      })
    }

    if (orderStatus.status === 'delivered') {
      res.json({
        msg: 'status delivered, cannot be modified'
      })
    } else {
      paymentStatus = 'confirmed'

      await db('order')
        .where('id', orderId)
        .update('status', paymentStatus)

      res.json({
        msg: `the status for ${orderId} is changed to ${paymentStatus}`
      })
    }
  } else if (data.result === 0) {
    const orderStatus = await db('order')
      .where('id', orderId)
      .first()

    if (orderStatus.status === 'delivered') {
      res.json({
        msg: 'status delivered, cannot be modified'
      })
    } else {
      paymentStatus = 'cancelled'

      await db('order')
        .where('id', orderId)
        .update({
          status: paymentStatus,
          deleted: moment().format()
        })


      res.json({
        msg: `the status for ${orderId} is changed to ${paymentStatus}`
      })
    }
  }
})


module.exports = router
