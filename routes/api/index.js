const express = require('express')
const router = express.Router()
const orderRoute = require('./order')

router.use('/order', orderRoute)
module.exports = router
